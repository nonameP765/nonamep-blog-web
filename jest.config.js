module.exports = {
  collectCoverageFrom: [
    "lib/**/*.{js,jsx,ts,tsx}",
    "pages/**/*.{js,jsx,ts,tsx}",
    "!**/*.d.ts",
    "!**/node_modules/**",
  ],
  coverageReporters: ["json"],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: [
    ".stories.tsx",
    "model",
    "icons",
    "pages",
    ".next",
    ".storybook",
    "coverage",
    "jest.config.js",
    "babel.config.js",
    "/api/",
    "/node_modules/",
  ],
  coverageThreshold: {
    global: {
      branches: 65,
      functions: 62,
      lines: 70,
      statements: 67,
    },
  },
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "^.+\\.(jpg|jpeg|png|gif|webp|svg)$": "<rootDir>/lib/__mocks__/fileMock.ts",
  },
  testPathIgnorePatterns: ["<rootDir>/node_modules/", "<rootDir>/.next/"],
  testEnvironment: "jsdom",
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": ["babel-jest", { presets: ["next/babel"] }],
  },
  transformIgnorePatterns: ["/node_modules/"],
  setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
};
