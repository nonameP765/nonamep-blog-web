import { render } from "@testing-library/react";
import Home from "@/pages/index";

describe("Home", () => {
  it("regression test", () => {
    const { baseElement } = render(<Home />);
    expect(baseElement).toMatchSnapshot();
  });
});
